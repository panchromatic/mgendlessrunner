﻿//
//	File Name	:	CoinController.cs
//	Description	:   CoinController : Hold coin data and raises coin collection events
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
	// Private member variables
	private int m_iValue;
	private const int m_iDefaultValue = 20;

	private void OnEnable()
	{
		GameSettings gameSettings = GameData.getInstance.m_gameSettings;

		if (gameSettings != null)
		{
			List<CoinType> validCoinTypes = new List<CoinType>();

			foreach (CoinType coinType in gameSettings.CoinTypes)
			{
				if (GameData.getInstance.m_fGameTime >= coinType.TimeAvaiable)
				{
					validCoinTypes.Add(coinType);
				}
			}

			int randomIndex = Random.Range(0, validCoinTypes.Count);

			m_iValue = validCoinTypes[randomIndex].Value;
			gameObject.GetComponent<SpriteRenderer>().color = validCoinTypes[randomIndex].Tint;

			// Game time please
			// make list of potiential coins (from settings time)
			// Get random index of coins
			// Set value and tint
		}
		else
		{
			m_iValue = m_iDefaultValue;
		}
	}

	/// Summary: On trigger enter with another 2D rigid body
	/// Returns: void
	void OnTriggerEnter2D(Collider2D _other)
	{
		//If colliding with coin add to score and delete coin
		if (_other.gameObject.tag == "Player")
		{
			// Invoke coin collection event.
			EventManager.getInstance.InvokeCoinCollect(m_iValue);
			AudioManager.m_instance.Play("CoinCollect");
			gameObject.SetActive(false);
		}
	}
}

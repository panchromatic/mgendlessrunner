﻿//
//	File Name	:	GameData.cs
//	Description	:   GameData : stores core game data
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
	// Game state enum for game flow management
	public enum EGAMESTATE
	{
		NONE,
		MAINMENU,
		GAMEPLAY,
		ENDGAME
	}

	// Static singleton instance.
	private static GameData instance;

	// Public member variables
	public int m_iScore;
	public EGAMESTATE eGameState;
	public GameObject m_player;
	public GameSettings m_gameSettings;
	public float m_fGameTime;

	/// Summary: Public get instance that creates and instance if null
	/// Returns: void
	public static GameData getInstance
	{
		get
		{
			if (instance == null)
			{
				instance = new GameData();
				instance.m_iScore = 0;
				instance.eGameState = EGAMESTATE.MAINMENU;
				instance.m_fGameTime = 0.0f;
				EventManager.coinCollect += instance.AddToScore;
			}
			return instance;
		}
	}

	/// Summary: Adds given amount to score
	/// Returns: void
	public void AddToScore(int _iAmount)
	{
		m_iScore += _iAmount;
	}
}

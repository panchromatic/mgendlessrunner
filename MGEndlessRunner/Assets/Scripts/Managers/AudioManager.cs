﻿//
//	File Name	:	AudioManager.cs
//	Description	:   AudioManager : stores sound settings and manages play requests
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using UnityEngine.Audio;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
	// Public member variables
	public AudioSettings m_audioSettings;

	// Static singleton instance
	public static AudioManager m_instance;

	/// Summary: Called when instantiated
	/// Returns: void
	void Awake()
    {
		if (m_instance == null)
		{
			m_instance = this;
		}
		else
		{
			Destroy(gameObject.GetComponent<AudioManager>());
			return;
		}	

		foreach(Sound s in m_audioSettings.m_sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.volume = s.volume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
			s.initialize();
		}
    }

	/// Summary: Called when activated for the first time
	/// Returns: void
	void Start()
	{
		if (GameData.getInstance.eGameState == GameData.EGAMESTATE.MAINMENU)
		{
			Play("MenuMusic");
		}
		else if (GameData.getInstance.eGameState == GameData.EGAMESTATE.GAMEPLAY)
		{
			Play("GameMusic");
		}
	}

	/// Summary: Plays a sound from sound settings based on the given string
	/// Returns: void
	public void Play(string _name)
	{
		Sound s = Array.Find(m_audioSettings.m_sounds, sound => sound.name == _name);

		if (s != null)
		{
			s.source.Play();

			if (s.subsequentPitchIncrease == true)
			{
				s.IncreaseInPitch();
			}
		}
		else
		{
			Debug.Log("Could not find sound");
		}
	}

	/// Summary: Called once per frame
	/// Returns: void
	void Update()
	{
		foreach (Sound s in m_audioSettings.m_sounds)
		{
			if (s.subsequentPitchIncrease == true &&
			s.subsequentPitchCount > 0)
			{	
				s.currentPitchDelayTimer += Time.deltaTime;

				if (s.currentPitchDelayTimer >= s.subsequentPitchDelayTimer)
				{
					s.subsequentPitchCount = 0;
					s.source.pitch = s.pitch;
				}
			}
		}
	}
}

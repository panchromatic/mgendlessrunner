﻿//
//	File Name	:	GameManager.cs
//	Description	:   GameManager : manages level initialization and game state
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public GameObject m_playerPrefab;
	public GameSettings m_gameSettings;
	public ObjectPoolSettings m_objectPoolSettings;

	/// Summary: Called when instantiated
	/// Returns: void
	private void Awake()
	{
		InitializeLevel();
	}

	/// Summary: Called when the game object is enabled
	/// Returns: void
	private void OnEnable()
	{
		EventManager.playerDeath += EndGameSession;
		EventManager.restartLevel += RestartLevel;
	}

	/// Summary: Called when the gameobject is disabled
	/// Returns: void
	private void OnDisable()
	{
		EventManager.playerDeath -= EndGameSession;
		EventManager.restartLevel -= RestartLevel;
	}

	void Update()
	{
		GameData.getInstance.m_fGameTime += Time.deltaTime;
	}

	/// Summary: Initializes level objects and parameters
	/// Returns: void
	private void InitializeLevel()
	{
		// Populate the object pool
		ObjectPoolManager.getInstance.PopulatePool(m_objectPoolSettings);

		// Set game data
		GameData.getInstance.eGameState = GameData.EGAMESTATE.GAMEPLAY;
		GameData.getInstance.m_gameSettings = m_gameSettings;
		GameData.getInstance.m_player = Instantiate(m_playerPrefab, new Vector3(0.0f, 2.0f, 0.0f), Quaternion.identity);
		GameData.getInstance.m_iScore = 0;

		// Start passive score 
		if (m_gameSettings.PassiveScore == true)
		{
			StartCoroutine(PassiveScoreTick());
		}
	}

	/// Summary: Sets the game state to end game
	/// Returns: void
	private void EndGameSession()
	{
		GameData.getInstance.eGameState = GameData.EGAMESTATE.ENDGAME;
	}

	/// Summary: Restarts the level
	/// Returns: void
	private void RestartLevel()
	{
		GameObject player = GameData.getInstance.m_player;

		// Reset the player's variables and position
		player.SetActive(true);
		player.transform.position = new Vector3(0.0f, 2.0f, 0.0f);
		player.GetComponent<PlayerMovement>().m_fSpeedIncrease = 0.0f;
		player.GetComponent<PlayerMovement>().m_bCanMove = true;

		// Reset game data
		GameData.getInstance.eGameState = GameData.EGAMESTATE.GAMEPLAY;
		GameData.getInstance.m_iScore = 0;
		GameData.getInstance.m_fGameTime = 0.0f;

		// Start passive score
		if (m_gameSettings.PassiveScore == true)
		{
			StartCoroutine(PassiveScoreTick());
		}
	}

	/// Summary: Adds to score based on timer
	/// Returns: void
	IEnumerator PassiveScoreTick()
	{
		while (GameData.getInstance.eGameState == GameData.EGAMESTATE.GAMEPLAY)
		{
			yield return new WaitForSeconds(m_gameSettings.PassiveScoreTickRate);
			GameData.getInstance.m_iScore += m_gameSettings.ScorePerPassiveTick;
		}
	}
}

﻿//
//	File Name	:	EventManager.cs
//	Description	:   EventManager : Library for events and invocation functions.
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager
{
	// Static singleton instance.
	private static EventManager instance;

	/// Summary: Public get instance that creates and instance if null
	/// Returns: void
	public static EventManager getInstance
	{
		get
		{
			if (instance == null)
			{
				instance = new EventManager();
			}
			return instance;
		}
	}

	// Coin collection event.
	public delegate void OnCoinCollect(int _iValue);
	public static event OnCoinCollect coinCollect;

	/// Summary: Invokes the coin collect event
	/// Returns: void
	public void InvokeCoinCollect(int _iValue)
	{
		coinCollect?.Invoke(_iValue);
	}

	// Player death event.
	public delegate void OnPlayerDeath();
	public static event OnPlayerDeath playerDeath;

	/// Summary: Invokes the player death event
	/// Returns: void
	public void InvokePlayerDeath()
	{
		playerDeath?.Invoke();
	}

	// Restart level event.
	public delegate void OnRestartLevel();
	public static event OnRestartLevel restartLevel;

	/// Summary: Invokes the level restart event
	/// Returns: void
	public void InvokeRestartLevel()
	{
		restartLevel?.Invoke();
	}
}

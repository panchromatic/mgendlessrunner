﻿//
//	File Name	:	ObjectPoolManager.cs
//	Description	:   ObjectPoolManager : Manages a pool of objects
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager
{
	// Public member variables
	public ObjectPoolSettings m_objectPoolSettings;
	public List<GameObject> m_pooledObjects;

	// Static singleton instance.
	private static ObjectPoolManager m_instance;

	/// Summary: Public get instance that creates and instance if null
	/// Returns: void
	public static ObjectPoolManager getInstance
	{
		get
		{
			if (m_instance == null)
			{
				m_instance = new ObjectPoolManager();
				m_instance.m_pooledObjects = new List<GameObject>();
			}
			return m_instance;
		}
	}

	/// Summary: Adds GameObjects to the pool based on the given pool settings
	/// Returns: void
	public void PopulatePool(ObjectPoolSettings _settings)
	{
		m_objectPoolSettings = _settings;
		
		foreach (ObjectPoolSettings.ObjectPoolItem item in m_objectPoolSettings.itemsToPool)
		{
			for (int i = 0; i < item.amountToPool; i++)
			{
				GameObject obj = Object.Instantiate(item.objectToPool) as GameObject;
				obj.SetActive(false);
				m_pooledObjects.Add(obj);
			}
		}
	}

	/// Summary: Returns an object based on given tag
	/// Returns: void
	public GameObject GetPooledObject(string tag)
	{
		// If there is an object in the pool that matches the tag then return it
		for (int i = 0; i < m_pooledObjects.Count; i++)
		{
			if (m_pooledObjects[i].tag == tag)
			{
				GameObject obj = m_pooledObjects[i];
				m_pooledObjects.RemoveAt(i);
				return obj;
			}
		}

		// Else check if there are settings for the next step
		if (m_objectPoolSettings == null)
		{
			Debug.Log("ObjectPoolSettings not Initialized");
			return null;
		}

		// Search the settings for the object with the same tag
		foreach (ObjectPoolSettings.ObjectPoolItem item in m_objectPoolSettings.itemsToPool)
		{
			if (item.objectToPool.tag == tag)
			{
				// If the object item is set to expand, instantiate a new one and add it to the pool
				if (item.shouldExpand)
				{
					GameObject obj = Object.Instantiate(item.objectToPool) as GameObject;
					obj.SetActive(false);
					return obj;
				}
			}
		}
		return null;
	}

	/// Summary: Adds an object to the pool
	/// Returns: void
	public void AddObjectToPool(GameObject _obj)
	{
		m_pooledObjects.Add(_obj);
	}
}

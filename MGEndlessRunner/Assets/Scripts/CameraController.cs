﻿//
//	File Name	:	CameraController.cs
//	Description	:   CameraController : updates camera position based on player position
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	/// Summary: Called once per frame
	/// Returns: void
	void Update()
    {
		GameObject player = GameData.getInstance.m_player;

		if (player != null)
		{
			transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
		}
	}
}

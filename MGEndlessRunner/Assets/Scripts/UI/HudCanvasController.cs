﻿//
//	File Name	:	HudCanvasController.cs
//	Description	:   HudCanvasController : determines visibility of canvas children and updates score text
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudCanvasController : MonoBehaviour
{
	// Public member variables
	public Text m_txtScore;

	// Private member variables
	private int m_iTextValue;
	private const string m_strScorePrefix = "Score: ";
	private bool m_bActive;

	/// Summary: Called when instantiated
	/// Returns: void
	void Awake()
	{
		m_iTextValue = 0;
		UpdateScoreText(0);
	}

	/// Summary: Called once per frame
	/// Returns: void
	void Update()
	{
		CheckScore();
		CheckGameState();
	}

	/// Summary: Start is called before the first frame update
	/// Returns: void
	private void CheckScore()
	{
		// Check to see if the score has been changed.
		int iScore = GameData.getInstance.m_iScore;

		if (m_iTextValue != iScore)
		{
			m_iTextValue = iScore;
			UpdateScoreText(iScore);
		}
	}

	/// Summary: Checks the state of the game to determine visibility
	/// Returns: void
	private void CheckGameState()
	{
		// Check if gamestate has changed.
		switch (GameData.getInstance.eGameState)
		{
			case GameData.EGAMESTATE.GAMEPLAY:
				if (m_bActive == false)
				{
					SetActive(true);
				}
				break;

			case GameData.EGAMESTATE.ENDGAME:
				if (m_bActive == true)
				{
					SetActive(false);
				}
				break;

			default:
				break;
		}
	}

	/// Summary: Sets the active property of all children in canvas
	/// Returns: void
	private void SetActive(bool _bActive)
	{
		m_bActive = _bActive;

		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(_bActive);
		}
	}

	/// Summary: Updates the score value of the score text
	/// Returns: void
	private void UpdateScoreText(int _iAmount)
	{
		m_txtScore.text = m_strScorePrefix + _iAmount.ToString();
	}
}

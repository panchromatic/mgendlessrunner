﻿//
//	File Name	:	EndCanvasController.cs
//	Description	:   EndCanvasController : determines visibility of canvas children and sets score text
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndCanvasController : MonoBehaviour
{
	// Public member variables
	public Text m_txtScore;

	// Private member variables
	private bool m_bActive;

	/// Summary: Called every frame
	/// Returns: void
	void Update()
	{
		CheckGameState();
	}

	/// Summary: Checks the state of the game to determine visibility
	/// Returns: void
	private void CheckGameState()
	{
		// Check if gamestate has changed
		switch (GameData.getInstance.eGameState)
		{
			case GameData.EGAMESTATE.GAMEPLAY:
				if (m_bActive == true)
				{
					SetActive(false);
				}
				break;

			case GameData.EGAMESTATE.ENDGAME:
				if (m_bActive == false)
				{
					SetActive(true);
					m_txtScore.text = GameData.getInstance.m_iScore.ToString();
				}
				break;

			default:
				break;
		}
	}

	/// Summary: Sets the active property of all children in canvas
	/// Returns: void
	private void SetActive(bool _bActive)
	{
		m_bActive = _bActive;

		for (int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(_bActive);
		}	
	}
}

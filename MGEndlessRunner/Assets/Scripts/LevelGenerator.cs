﻿//
//	File Name	:	LevelGenerator.cs
//	Description	:   LevelGenerator : randomly generates the level platforms
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
	// Declare private Member Variables
	private const float m_kfPlayerPlatSpawnDist = 20.0f;
	private const float m_kfPlayerPlatDeSpawnDist = 25.0f;
	private const float m_kfCheckPlatformTime = 1.0f;
	private Vector2 m_vec2LastEndPosition;
	private float m_fCurrentPlatformCheckTime;

	// Declare public Member Variables
	//public GameObject[] m_platformPrefabs;
	public string m_startPlatformTag;
	public GameObject m_startPlatformPrefab;
	public string[] m_platformTags;
	//private List<GameObject> m_inActivePlatforms;
	private List<GameObject> m_activePlatforms;

	/// Summary: Start is called before the first frame update
	/// Returns: void
	void Start()
    {
		// Define private member variables
		m_fCurrentPlatformCheckTime = 0.0f;
		//m_inActivePlatforms = new List<GameObject>();	
		m_activePlatforms = new List<GameObject>();
		Initialize();
	}

	/// Summary: Called when instantiated
	/// Returns: void
	void Awake()
	{
		EventManager.restartLevel += Initialize;
	}

	/// Summary: Called when being destroyed
	/// Returns: void
	void OnDestroy()
	{
		EventManager.restartLevel -= Initialize;
	}

	/// Summary: Disables all active platforms and creates the first platform
	/// Returns: void
	private void Initialize()
	{
		for (int i = 0; i < m_activePlatforms.Count; i++)
		{
			m_activePlatforms[i].SetActive(false);
			ObjectPoolManager.getInstance.AddObjectToPool(m_activePlatforms[i]);
		}
		m_activePlatforms.Clear();

		GameObject firstPlatform = ObjectPoolManager.getInstance.GetPooledObject(m_startPlatformTag);

		if (firstPlatform != null)
		{
			firstPlatform.SetActive(true);
			firstPlatform.transform.position = new Vector3(0.0f, 0.0f, 0.0f);
		}
		else if (firstPlatform == null && m_startPlatformPrefab != null)
		{
			firstPlatform = Instantiate(m_startPlatformPrefab, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity);
		}
		else
		{
			Debug.Log("Error: both first platform tag and firstplatform prefab are invalid in LevelGenerator");
		}

		
		m_vec2LastEndPosition = firstPlatform.transform.Find("EndPosition").transform.position;
		m_activePlatforms.Add(firstPlatform);
	}

	/// Summary: Update is called once per frame
	/// Returns: void
	void Update()
    {
		GameObject player = GameData.getInstance.m_player;

		if (player != null)
		{
			// If the player is close to the end of the last platform, spawn another platform
			if (Vector2.Distance(player.transform.position, m_vec2LastEndPosition) < m_kfPlayerPlatSpawnDist)
			{
				SpawnRandomPlatform();
			}
		}

		// Check for active platform positions on a set timer
		if (m_fCurrentPlatformCheckTime >= m_kfCheckPlatformTime)
		{
			CheckActivePlatformsPos(player);
			m_fCurrentPlatformCheckTime = 0.0f;
		}
		else
		{
			m_fCurrentPlatformCheckTime += Time.deltaTime;
		}
	}

	/// Summary: Spawns a platform and sets new end position
	/// Returns: void
	private void SpawnRandomPlatform()
	{
		int randomPlatformIndex = Random.Range(0, m_platformTags.Length);
		GameObject platform = ObjectPoolManager.getInstance.GetPooledObject(m_platformTags[randomPlatformIndex]);

		if (platform != null)
		{
			m_activePlatforms.Add(platform);
			platform.SetActive(true);
			
			foreach (GameObject coin in platform.GetComponent<PlatformController>().m_coins)
			{
				coin.SetActive(true);
			}

			platform.transform.position = m_vec2LastEndPosition;

			m_vec2LastEndPosition = platform.GetComponent<PlatformController>().m_endPosition.transform.position;
		}
		else
		{
			Debug.Log("Attempting to create platform with tag that does not exist in the object pool");
		}

		//// If there is a platform of the same tag in pool then use that one
		//for (int i = 0; i < m_inActivePlatforms.Count; i++)
		//{
		//	if (m_platformPrefabs[randomPlatformIndex].tag == m_inActivePlatforms[i].tag)
		//	{
		//		m_inActivePlatforms[i].SetActive(true);
		//		m_inActivePlatforms[i].transform.Find("Coin").gameObject.SetActive(true);
		//		m_inActivePlatforms[i].transform.position = m_vec2LastEndPosition;
		//		AddNewPlatformPosition(m_inActivePlatforms[i]);
		//		m_inActivePlatforms.RemoveAt(i);
		//		return;
		//	}
		//}

		//// Else create a new platform
		//GameObject newPlatform = Instantiate(m_platformPrefabs[randomPlatformIndex], m_vec2LastEndPosition, Quaternion.identity);
		//AddNewPlatformPosition(newPlatform);
	}

	/// Summary: Checks if any active playforms are too far away from the player,
	///			 adds them to the inactive pool if they are
	/// Returns: void
	public void CheckActivePlatformsPos(GameObject _targetObj)
	{
		Vector2 vec2PlatformEndPosition;

		for (int i = 0; i < m_activePlatforms.Count; i++)
		{
			vec2PlatformEndPosition = m_activePlatforms[i].transform.position;

			if (_targetObj != null)
			{
				if (Vector2.Distance(_targetObj.transform.position, vec2PlatformEndPosition) > m_kfPlayerPlatDeSpawnDist)
				{
					m_activePlatforms[i].SetActive(false);
					ObjectPoolManager.getInstance.AddObjectToPool(m_activePlatforms[i]);
					m_activePlatforms.RemoveAt(i);
				}
			}
		}
	}

	///// Summary: Sets the position of the given platform and adds it to the active pool
	///// Returns: void
	//private void AddNewPlatformPosition(GameObject _platformObject)
	//{	
	//	m_vec2LastEndPosition = _platformObject.transform.Find("EndPosition").transform.position;
	//	m_activePlatforms.Add(_platformObject);
	//}
}

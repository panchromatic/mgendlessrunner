﻿//
//	File Name	:	PlayerMovement.cs
//	Description	:   PlayerMovement : handles player movement and user input
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	// Enum for general input
	private enum USERINPUT
	{
		INPUT_DOWN,
		INPUT_UP,
		INPUT_HOLD
	}
	
	// Private Member Variables
	private Rigidbody2D m_rigidBody;
	private bool m_bIsJumping;
	private float m_fCurrentJumpTime;
	private const float m_kfGroundCheckDist = 0.5f;

	[HideInInspector]
	public bool m_bCanMove;
	[HideInInspector]
	public float m_fSpeedIncrease;

	private Animator m_animator;

	// Public Member Variables
	public GameSettings m_playerSettings;

	/// Summary: Start is called before the first frame update
	/// Returns: void
	void Start()
	{
		// Define private member variables
		m_rigidBody = GetComponent<Rigidbody2D>();
		m_bIsJumping = false;
		m_fCurrentJumpTime = 0.0f;
		m_bCanMove = true;
		m_animator = GetComponent<Animator>();
		m_fSpeedIncrease = 0.0f;	
	}

	/// Summary: Called when enabled
	/// Returns: void
	private void OnEnable()
	{
		EventManager.playerDeath += SetCantMove;
	}

	/// Summary: Called when disabled
	/// Returns: void
	private void OnDisable()
	{
		EventManager.playerDeath -= SetCantMove;
	}

	/// Summary: FixedUpdate is called once per fixed framerate frame
	/// Returns: void
	void FixedUpdate()
	{
		// Move the player on the horizontal axis
		if (m_bCanMove)
		{
			m_rigidBody.velocity = new Vector2(m_playerSettings.InitialSpeed + m_fSpeedIncrease, m_rigidBody.velocity.y);
		}

		// If the player is not jumping and their y velocity is negative then
		// set the animator to falling
		if (m_bIsJumping == false && m_rigidBody.velocity.y < -1.0f)
		{
			m_animator.SetBool("Falling", true);
		}
		else
		{
			m_animator.SetBool("Falling", false);
		}

		// Set the speed of the animator based on movement speed
		m_animator.speed = (m_playerSettings.InitialSpeed + m_fSpeedIncrease) / 2.0f;
	}

	/// Summary: called once per frame
	/// Returns: void
	void Update()
	{
		if (m_bCanMove)
		{
			CheckForUserInput();
		}

		m_fSpeedIncrease += m_playerSettings.SpeedIncreasePerSecond * Time.deltaTime;
	}

	/// Summary: Ray casts for a ground object 
	/// Returns: Returns true if a ground object was hit
	private bool IsGrounded()
	{
		// Raycast below the player's feet for the ground
		RaycastHit2D hitResult = Physics2D.Raycast
		(
			m_rigidBody.position,
			-Vector2.up,
			m_kfGroundCheckDist,
			m_playerSettings.GroundLayer
		);

		// If the ground was hit
		if (hitResult.collider != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// Summary: Checks for user input
	/// Returns: Void
	private void CheckForUserInput()
	{
		// If screen is touched check for touch events,
		// else check for space bar press.
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Began)
			{
				UpdateJump(USERINPUT.INPUT_DOWN);
			}
			else if (touch.phase == TouchPhase.Ended)
			{
				UpdateJump(USERINPUT.INPUT_UP);
			}
			else if (touch.phase != TouchPhase.Canceled)
			{
				UpdateJump(USERINPUT.INPUT_HOLD);
			}
		}
		else
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				UpdateJump(USERINPUT.INPUT_DOWN);
			}
			else if (Input.GetKeyUp(KeyCode.Space))
			{
				UpdateJump(USERINPUT.INPUT_UP);
			}
			else if (Input.GetKey(KeyCode.Space))
			{
				UpdateJump(USERINPUT.INPUT_HOLD);
			}
		}
	}

	/// Summary: Updates jumping based on given user input
	/// Returns: Void
	private void UpdateJump(USERINPUT _eInput)
	{
		if (IsGrounded() == true && _eInput == USERINPUT.INPUT_DOWN)
		{
			m_animator.SetBool("Jumping", true);
			AudioManager.m_instance.Play("Jump");
			m_bIsJumping = true;
			m_fCurrentJumpTime = 0.0f;
			m_rigidBody.velocity = Vector2.up * m_playerSettings.JumpForce;
		}

		// Continue to launch into the air while holding jump input,
		// and the max jump time has not been met
		if (m_bIsJumping == true && _eInput == USERINPUT.INPUT_HOLD)
		{
			if (m_fCurrentJumpTime < m_playerSettings.MaxJumpTime)
			{
				m_rigidBody.velocity = Vector2.up * m_playerSettings.JumpForce;
				m_fCurrentJumpTime += Time.deltaTime;
			}
			else
			{
				m_animator.SetBool("Jumping", false);
				m_bIsJumping = false;
			}
		}

		// If the player jump input is released then stop jumping
		if (_eInput == USERINPUT.INPUT_UP)
		{
			m_animator.SetBool("Jumping", false);
			m_bIsJumping = false;
		}
	}

	/// Summary: Sets if the player can move
	/// Returns: Void
	public void SetCantMove()
	{
		m_bCanMove = false;
	}
}

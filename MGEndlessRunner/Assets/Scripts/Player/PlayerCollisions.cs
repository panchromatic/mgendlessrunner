﻿//
//	File Name	:	PlayerCollisions.cs
//	Description	:   PlayerCollisions : handles player collisions
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCollisions : MonoBehaviour
{
	// Declare public variables
	public GameSettings m_playerSettings;

	// Declare private variables
	private Rigidbody2D m_rigidBody;
	private const float m_kfFrontCheckDist = 0.5f;

	/// Summary: Start is called before the first frame update
	/// Returns: void
	void Start()
	{
		// Define private member variables
		m_rigidBody = GetComponent<Rigidbody2D>();
	}

	/// Summary: On collision enter with another 2D rigid body
	/// Returns: void
	void OnCollisionEnter2D(Collision2D _other)
	{
		// If colliding with water end the run
		if (_other.collider.tag == "Water")
		{
			AudioManager.m_instance.Play("WaterSplash");
			EventManager.getInstance.InvokePlayerDeath();
			gameObject.SetActive(false);
		}
		else if (_other.collider.tag == "Ground")
		{
			CheckFrontCollisions();
		}
	}

	/// Summary: Checks to see if there has been a collision at the front of the player
	/// Returns: void
	private void CheckFrontCollisions()
	{
		float fVerticalCheckOffset = GetComponent<SpriteRenderer>().bounds.size.y / 4.0f;

		for (int i = 0; i < 2; i++)
		{
			fVerticalCheckOffset = -fVerticalCheckOffset;

			// Raycast 
			RaycastHit2D hitResult = Physics2D.Raycast
			(
				m_rigidBody.position + new Vector2(0.0f, fVerticalCheckOffset),
				Vector2.right,
				m_kfFrontCheckDist,
				m_playerSettings.GroundLayer
			);

			// If face was hit
			if (hitResult.collider != null)
			{
				AudioManager.m_instance.Play("Death");
				EventManager.getInstance.InvokePlayerDeath();
				gameObject.SetActive(false);
			}
		}
	}
}

﻿//
//	File Name	:	Parallax.cs
//	Description	:   Parallax : Positions a spite based on camera position and
//							   parallax strength.
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
	// Declare private member variables
	private float m_fLength;
	private float m_fStartpos;
	private float m_fCurrentPos;

	// Declare public member variables
	public GameObject m_Camera;
	public float m_fParallaxStrength;

	/// Summary: Start is called before the first frame update
	/// Returns: void
	void Start()
	{
		// Define private member variables
		m_fStartpos = transform.position.x;
		m_fCurrentPos = m_fStartpos;
		m_fLength = GetComponent<SpriteRenderer>().bounds.size.x;
	}

	private void OnEnable()
	{
		EventManager.restartLevel += ResetPosition;
	}

	/// Summary: Called when the gameobject is disabled
	/// Returns: void
	private void OnDisable()
	{
		EventManager.restartLevel -= ResetPosition;
	}

	/// Summary: FixedUpdate is called once per fixed framerate frame
	/// Returns: void
	void FixedUpdate()
    {
		float temp = (m_Camera.transform.position.x * (1 - m_fParallaxStrength));
		float dist = (m_Camera.transform.position.x * m_fParallaxStrength);

		transform.position = new Vector3(m_fCurrentPos + dist, transform.position.y, transform.position.z);

		if (temp > m_fCurrentPos + m_fLength)
		{
			m_fCurrentPos += m_fLength;
		}
		else if (temp < m_fCurrentPos - m_fLength)
		{
			m_fCurrentPos -= m_fLength;
		}
    }

	/// Summary: Sets the current position to the start position
	/// Returns: void
	private void ResetPosition()
	{
		m_fCurrentPos = m_fStartpos;
	}
}

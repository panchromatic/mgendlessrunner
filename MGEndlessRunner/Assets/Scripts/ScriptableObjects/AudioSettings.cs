﻿//
//	File Name	:	AudioSettings.cs
//	Description	:   AudioSettings : contains array of sounds
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAudioSettings", menuName = "ScriptableObjects/GameSettings/AudioSettings")]
public class AudioSettings : ScriptableObject
{
	// Public member variables
	public Sound[] m_sounds;
}

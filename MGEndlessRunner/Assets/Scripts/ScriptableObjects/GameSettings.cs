﻿//
//	File Name	:	CharacterSettings.cs
//	Description	:   CharacterSettings : contains various information about a character
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Serialization;
using UnityEngine;

[CreateAssetMenu(fileName = "NewGameSettings", menuName = "ScriptableObjects/GameSettings/GameSettings")]
public class GameSettings : ScriptableObject
{
	// Public member variables
	[Header("Player Settings")]
	[Range(0.0f, 8.0f)]
	public float InitialSpeed;
	[Range(0.0f, 0.2f)]
	public float SpeedIncreasePerSecond;
	[Range(0.0f, 15.0f)]
	public float JumpForce;
	[Tooltip("Maximum amount of time the jump force can accumulate")]
	[Range(0.0f, 0.7f)]
	public float MaxJumpTime;
	public LayerMask GroundLayer;

	[Header("GameMode Settings")]
	public bool PassiveScore;
	public float PassiveScoreTickRate;
	public int ScorePerPassiveTick;

	[Header("Coin Settings")]
	public CoinType[] CoinTypes;
}

﻿//
//	File Name	:	ObjectPoolSettings.cs
//	Description	:   ObjectPoolSettings : Stores object pool information
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewObjectPoolSettings", menuName = "ScriptableObjects/GameSettings/ObjectPoolSettings")]
public class ObjectPoolSettings : ScriptableObject
{
	// Public member variables
	[System.Serializable]
	public class ObjectPoolItem
	{
		public int amountToPool;
		public GameObject objectToPool;
		public bool shouldExpand;
	}

	public List<ObjectPoolItem> itemsToPool;
}

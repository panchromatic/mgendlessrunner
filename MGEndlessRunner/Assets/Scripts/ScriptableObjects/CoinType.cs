﻿//
//	File Name	:	CoinType.cs
//	Description	:   CoinType : contains various information about a coin
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCoinType", menuName = "ScriptableObjects/CoinType")]
public class CoinType : ScriptableObject
{
	// Public member variables
	public int Value;
	public Color Tint;
	[Tooltip("How long until this coin can start to appear")]
	public float TimeAvaiable;
}

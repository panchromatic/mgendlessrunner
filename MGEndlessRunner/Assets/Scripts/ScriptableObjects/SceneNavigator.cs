﻿//
//	File Name	:	SceneNavigator.cs
//	Description	:   SceneNavigator : determines what scene to load based on given scenes
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "NewSceneNavigator", menuName = "ScriptableObjects/SceneNavigator")]
public class SceneNavigator : ScriptableObject
{
	// Declare variables;
	[Header("Scenes")]
	public string MainMenu;
	public string GameScene;

	/// Summary: When called load the GameScene
	/// Returns: void
	public void LoadGameScene()
	{
		AudioManager.m_instance.Play("ButtonClick");
		SceneManager.LoadScene(GameScene);
	}

	/// Summary: When called load the MenuScene
	/// Returns: void
	public void LoadMainMenuScene()
	{
		AudioManager.m_instance.Play("ButtonClick");
		GameData.getInstance.eGameState = GameData.EGAMESTATE.MAINMENU;
		ObjectPoolManager.getInstance.m_pooledObjects.Clear();
		SceneManager.LoadScene(MainMenu);		
	}

	/// Summary: When called exit the game
	/// Returns: void
	public void ExitApplication()
	{
		Application.Quit();
	}

	/// Summary: When called invokes game restart event
	/// Returns: void
	public void RestartLevel()
	{
		AudioManager.m_instance.Play("ButtonClick");
		EventManager.getInstance.InvokeRestartLevel();
	}
}

﻿//
//	File Name	:	PlatformController.cs
//	Description	:   PlatformController : stores GameObject information about platform's children
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
	// Public member variables
	public GameObject m_endPosition;
	public GameObject[] m_coins;
}

﻿//
//	File Name	:	Sound.cs
//	Description	:   Sound : stores information about a sound
//	Author		:	Mati Green
//	Mail		:	matitahi@gmail.com
//
using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound
{
	// Name of sound
	public string name;

	// Audio clip to manage
	public AudioClip clip;

	[Range(0f, 1f)]
	public float volume;

	[Range(0.1f, 3.0f)]
	public float pitch;

	public bool loop;

	[HideInInspector]
	public AudioSource source;

	public bool subsequentPitchIncrease;
	public float subsequentPitchDelayTimer;

	[HideInInspector]
	public int subsequentPitchCount;
	[HideInInspector]
	public int maxPitchCountIncrease;
	[HideInInspector]
	public float currentPitchDelayTimer;

	/// Summary: Initializes default values for pitch increase
	/// Returns: void
	public void initialize()
	{
		subsequentPitchCount = 0;
		maxPitchCountIncrease = 8;	
		currentPitchDelayTimer = 0.0f;
	}

	/// Summary: Increments pitch of sound
	/// Returns: void
	public void IncreaseInPitch()
	{
		if (subsequentPitchCount < maxPitchCountIncrease)
		{
			source.pitch += 0.1f;
			subsequentPitchCount++;
		}

		currentPitchDelayTimer = 0.0f;
	}

}
